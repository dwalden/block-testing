#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright (c) 2023 Dominic Walden
# Copyright (c) 2003-2021 Pywikibot team

# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this software and associated documentation
# files (the "Software"), to deal in the Software without
# restriction, including without limitation the rights to use,
# copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following
# conditions:

# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.

import argparse
import csv
import random
import string
import pywikibot

from urllib.parse import quote
from datetime import datetime
from pywikibot.comms import http


class AllActionsUser(object):

    """TODO"""

    def __init__(self, *args):
        """Initializer."""
        self.set_options(*args)
        self.site = pywikibot.Site()

    def set_options(self, *args):
        """Parse commandline and set options attribute."""
        my_args = pywikibot.handle_args(args)
        parser = argparse.ArgumentParser()
        parser.add_argument('-i', '--ips', nargs='*', required=True)
        parser.add_argument('-a', '--anonymous', action='store_true')
        self.options = parser.parse_args(my_args)

    def run(self):
        """Run the bot."""
        if self.options.anonymous:
            self.site.logout()
        else:
            self.site.login()

        fields = ['user', 'ip', 'action', 'output', 'debug', 'params']
        rows = []

        for ip in self.options.ips:
            headers = {'X-Forwarded-For': ip}

            tokens = self.site.get_tokens(["csrf", "createaccount", "rollback", "deleteglobalaccount"])
            csrf = tokens.get("csrf")
            catoken = tokens.get("createaccount")
            rollbacktoken = tokens.get("rollback")
            deleteglobalaccounttoken = tokens.get("deleteglobalaccount")

            delete_page = list(self.site.randompages(total=1, namespaces='0'))[0]
            rollback_page = list(self.site.randompages(total=1, namespaces='0'))[0]
            # flow_page = list(self.site.randompages(total=1, namespaces='2600'))
            newuser = "User {}".format(''.join(random.choice(string.ascii_lowercase) for i in range(10)))
            actions = [
                {'action': 'createaccount',
                 'username': newuser,
                 'password': "urelsjfkldrujisedko",
                 'retype': "urelsjfkldrujisedko",
                 'createreturnurl': "http://foo.com",
                 'format': 'json',
                 'createtoken': "+\\" if catoken is None else catoken},
                {'action': 'deleteglobalaccount',
                 'user': newuser,
                 'reason': ip,
                 'format': 'json',
                 'token': "+\\" if deleteglobalaccounttoken is None else deleteglobalaccounttoken},
                {'action': 'edit',
                 'custom_name': 'create',
                 'title': "Page {} {}".format(ip, ''.join(random.choice(string.ascii_lowercase) for i in range(10))),
                 'appendtext': "create {}".format(ip),
                 'summary': "{}".format(ip),
                 'format': 'json',
                 'token': "+\\" if csrf is None else csrf},
                {'action': 'edit',
                 'title': "All_Actions_User",
                 'appendtext': "edit {}".format(ip),
                 'summary': "{}".format(ip),
                 'format': 'json',
                 'token': "+\\" if csrf is None else csrf},
                {'action': 'block',
                 'user': "2.3.4.5",
                 'reason': ip,
                 'format': 'json',
                 'token': "+\\" if csrf is None else csrf},
                {'action': 'block',
                 'custom_name': 'reblock',
                 'user': "2.3.4.5",
                 'nocreate': True,
                 'reblock': True,
                 'reason': ip,
                 'format': 'json',
                 'token': "+\\" if csrf is None else csrf},
                {'action': 'unblock',
                 'user': "2.3.4.5",
                 'reason': ip,
                 'format': 'json',
                 'token': "+\\" if csrf is None else csrf},
                {'action': 'globalblock',
                 'target': "2.3.4.5",
                 'expiry': "infinite",
                 'reason': ip,
                 'format': 'json',
                 'token': "+\\" if csrf is None else csrf},
                {'action': 'globalblock',
                 'custom_name': "globalreblock",
                 'target': "2.3.4.5",
                 'expiry': "1 day",
                 'anononly': True,
                 'modify': True,
                 'reason': ip,
                 'format': 'json',
                 'token': "+\\" if csrf is None else csrf},
                {'action': 'globalblock',
                 'custom_name': 'globalunblock',
                 'target': "2.3.4.5",
                 'unblock': True,
                 'reason': ip,
                 'format': 'json',
                 'token': "+\\" if csrf is None else csrf},
                {'action': 'changecontentmodel',
                 'title': list(self.site.randompages(total=1, namespaces='0'))[0].title(),
                 'model': 'text',
                 'summary': "{}".format(ip),
                 'format': 'json',
                 'token': "+\\" if csrf is None else csrf},
                {'action': 'delete',
                 'title': delete_page.title(),
                 'reason': ip,
                 'format': 'json',
                 'token': "+\\" if csrf is None else csrf},
                {'action': 'undelete',
                 'title': delete_page.title(),
                 'reason': ip,
                 'format': 'json',
                 'token': "+\\" if csrf is None else csrf},
                {'action': 'discussiontoolsedit',
                 'paction': 'addtopic',
                 'page': "User:{}".format(list(self.site.randompages(total=1, namespaces='0'))[0].title()),
                 'wikitext': "Discussion comment {} {}".format(ip, ''.join(random.choice(string.ascii_lowercase) for i in range(10))),
                 'sectiontitle': "Discussion topic {} {}".format(ip, ''.join(random.choice(string.ascii_lowercase) for i in range(10))),
                 'summary': "{}".format(ip),
                 'format': 'json',
                 'token': "+\\" if csrf is None else csrf},
                {'action': 'emailuser',
                 'target': 'Admin',
                 'subject': 'test',
                 'text': 'test',
                 'format': 'json',
                 'token': "+\\" if csrf is None else csrf},
                # {'action': 'flow',
                #  'submodule': 'new-topic',
                #  'page': "" if len(flow_page) == 0 else flow_page[0].title(),
                #  'nttopic': 'New topic {} {}'.format(ip, ''.join(random.choice(string.ascii_lowercase) for i in range(10))),
                #  'ntcontent': 'New content {} {}'.format(ip, ''.join(random.choice(string.ascii_lowercase) for i in range(10))),
                #  'format': 'json',
                #  'token': "+\\" if csrf is None else csrf},
                # {'action': 'flow',
                #  'submodule': 'reply',
                #  'page': "" if len(flow_page) == 0 else flow_page[0].title(),
                #  'nttopic': 'New topic {} {}'.format(ip, ''.join(random.choice(string.ascii_lowercase) for i in range(10))),
                #  'ntcontent': 'New content {} {}'.format(ip, ''.join(random.choice(string.ascii_lowercase) for i in range(10))),
                #  'format': 'json',
                #  'token': "+\\" if csrf is None else csrf},
                {'action': 'move',
                 'from': list(self.site.randompages(total=1, namespaces='0'))[0].title(),
                 'to': "Page {}".format(''.join(random.choice(string.ascii_lowercase) for i in range(10))),
                 'reason': ip,
                 'format': 'json',
                 'token': "+\\" if csrf is None else csrf},
                {'action': 'protect',
                 'title': list(self.site.randompages(total=1, namespaces='0'))[0].title(),
                 'protections': '',
                 'reason': ip,
                 'format': 'json',
                 'token': "+\\" if csrf is None else csrf},
                {'action': 'revisiondelete',
                 'type': 'revision',
                 'ids': list(self.site.randompages(total=1, namespaces='0'))[0].oldest_revision['revid'],
                 'hide': 'content',
                 'reason': ip,
                 'format': 'json',
                 'token': "+\\" if csrf is None else csrf},
                {'action': 'rollback',
                 'title': rollback_page.title(),
                 'user': rollback_page.userName(),
                 'reason': ip,
                 'format': 'json',
                 'token': "+\\" if rollbacktoken is None else rollbacktoken},
                {'action': 'managetags',
                 'operation': 'create',
                 'tag': "test tag",
                 'format': 'json',
                 'token': "+\\" if csrf is None else csrf},
                {'action': 'tag',
                 'revid': list(self.site.randompages(total=1, namespaces='0'))[0].latest_revision_id,
                 'tags': "test tag",
                 'format': 'json',
                 'token': "+\\" if csrf is None else csrf},
                {'action': 'thank',
                 'rev': list(self.site.randompages(total=1, namespaces='0'))[0].latest_revision_id,
                 'format': 'json',
                 'token': "+\\" if csrf is None else csrf},
                {'action': 'upload',
	         'filename': 'wiki_{}.png'.format(''.join(random.choice(string.ascii_lowercase) for i in range(5))),
	         'ignorewarnings': 1,
	         'url': 'http://mediawiki-web:8080/w/resources/assets/wiki.png',
                 'format': 'json',
                 'token': "+\\" if csrf is None else csrf}
            ]

            for params in actions:
                if 'custom_name' in params:
                    action_name = params['custom_name']
                    del params['custom_name']
                else:
                    action_name = params['action']
                output = http.session.post("{}://{}{}/api.php".format(self.site.protocol(), self.site.family.langs[self.site.code], self.site.scriptpath()), data=params, headers=headers)
                row = {'user': "anonymous" if self.options.anonymous else self.site.username(),
                       'ip': ip,
                       'action': action_name,
                       'output': output.json()['error']['code'] if 'error' in output.json() else 'success' if params['action'] != 'createaccount' else output.json()['createaccount']['status'],
                       'debug': output.json(),
                       'params': params}
                rows.append(row)
                if self.options.anonymous:
                    self.site.logout()

        with open("all_actions_user_{}_{}_{}.csv".format(self.site.family.langs[self.site.code], "anonymous" if self.options.anonymous else quote(self.site.username()), datetime.now().strftime("%Y-%m-%dT%H:%M:%S")), 'a', newline='') as csvfile:
            dictcsv = csv.DictWriter(csvfile, fields, restval=False, extrasaction="ignore")
            dictcsv.writeheader()
            dictcsv.writerows(rows)


def main(*args):
    """
    Process command line arguments and invoke bot.

    If args is an empty list, sys.argv is used.

    @param args: command line arguments
    @type args: str
    """
    app = AllActionsUser(*args)
    app.run()


if __name__ == '__main__':
    main()
