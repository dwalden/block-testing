#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright (c) 2023 Dominic Walden
# Copyright (c) 2003-2022 Pywikibot team

# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this software and associated documentation
# files (the "Software"), to deal in the Software without
# restriction, including without limitation the rights to use,
# copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following
# conditions:

# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.


import argparse
import csv
import pywikibot

from datetime import datetime
from itertools import combinations


class Block(object):

    """Block user, IP or IP range."""

    def __init__(self, *args):
        """Initializer."""
        self.set_options(*args)
        self.site = pywikibot.Site()

    def set_options(self, *args):
        """Parse commandline and set options attribute."""
        my_args = pywikibot.handle_args(args)
        parser = argparse.ArgumentParser()
        parser.add_argument('-r', '--reason', required=True)
        parser.add_argument('-n', '--nodelay', action='store_true')
        self.options = parser.parse_args(my_args)

    def run(self):
        """Run the bot."""
        if self.options.nodelay:
            self.site.throttle.setDelays(0, 0, True)
        self.site.login()

        all_blocks = [
            {'type': 'database',
             'anon': True},
            {'type': 'database',
             'anon': False},
            {'type': 'global',
             'anon': True},
            {'type': 'global',
             'anon': False},
            {'type': 'system',
             'anon': True},
            {'type': 'system',
             'anon': False},
        ]

        fields = ['ip', 'type', 'anon']
        rows = []

        all_block_combinations = []
        for r in range(1, len(all_blocks) + 1):
            all_block_combinations.extend(combinations(all_blocks, r))

        first_octet = 1
        wgSoftBlockRanges = []
        wgProxyList = []
        for blocks in all_block_combinations:
            target = "{}.1.1.1".format(first_octet)
            first_octet = first_octet + 1
            cidr_range = 32
            for block in blocks:
                row = {'ip': target,
                       'type': block['type'],
                       'anon': block['anon']}
                rows.append(row)
                if block['type'] == 'database':
                    self.site.blockuser(user=pywikibot.User(self.site, "{}/{}".format(target, cidr_range)), anononly=block['anon'], expiry="indefinite", reason=self.options.reason, noemail=True, reblock=True)
                    cidr_range = cidr_range - 1
                elif block['type'] == 'global':
                    req = self.site.simple_request(action='globalblock', target="{}/{}".format(target, cidr_range), anononly=block['anon'], expiry="indefinite", reason=self.options.reason, modify=True, token=self.site.get_tokens(["csrf"]).get("csrf"))
                    req.submit()
                    cidr_range = cidr_range - 1
                else:
                    if block['anon']:
                        wgSoftBlockRanges.append(target)
                    else:
                        wgProxyList.append(target)
            pywikibot.output("{}: {}".format(target, blocks))

        pywikibot.output("$wgSoftBlockRanges = {};".format(wgSoftBlockRanges))
        pywikibot.output("$wgProxyList = {};".format(wgProxyList))

        with open("all_block_combinations_{}_{}.csv".format(self.site.family.langs[self.site.code], datetime.now().strftime("%Y-%m-%dT%H:%M:%S")), 'a', newline='') as csvfile:
            dictcsv = csv.DictWriter(csvfile, fields, restval=False, extrasaction="ignore")
            dictcsv.writeheader()
            dictcsv.writerows(rows)


def main(*args):
    """
    Process command line arguments and invoke bot.

    If args is an empty list, sys.argv is used.

    @param args: command line arguments
    @type args: str
    """
    app = Block(*args)
    response = app.run()
    pywikibot.output(response)


if __name__ == '__main__':
    main()
