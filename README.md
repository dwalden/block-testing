# block-testing

Useful scripts when testing blocks.

## `all_block_combinations.py`

Generates combinations of blocks against IPs on a wiki.

1. Setup pywikibot using the instructions here https://www.mediawiki.org/wiki/Manual:Pywikibot/Installation
2. Copy `all_block_combinations.py` from this repository into the `scripts/` directory in pywikibot
3. Run this command:
```
python3 pwb.py all_block_combinations -r "Testing" -lang:en -family:dockerwiki
```
4. The last two lines of output will be something like below. Copy these two lines from the output into your `LocalSettings.php`:
```
$wgSoftBlockRanges = ['5.1.1.1', ...
$wgProxyList = ['6.1.1.1', ...
```
5. It will also generate a CSV file, which may be useful later, called something like `all_block_combinations_<wiki>_<date>.csv`

## `all_actions_user.py`

Tries to perform a variety of actions from a list of IPs, either anonymously or logged in, and records the result (e.g. whether or not they were blocked).

Useful to test if an IP has been correctly blocked. Useful if used in combination with `all_block_combinations.py`.

1. Follow the instructions for `all_block_combinations.py`
2. Setup pywikibot (if you have not already done so) using the instructions here https://www.mediawiki.org/wiki/Manual:Pywikibot/Installation
3. Copy `all_actions_user.py` from this repository into the `scripts/` directory in pywikibot
4. Add this to your `LocalSettings.php`. This assumes you are using docker. If you are running mediawiki on bare metal, instead replace the first line with `$wgCdnServers = [ '127.0.0.1' ];`.
```
$wgCdnServersNoPurge = [ '172.0.0.1/8' ];
$wgUsePrivateIPs = true;
```
5. (Optional, but recommended) In addition, add the below to your `LocalSettings.php`. The script works by switching IP addresses. If cookie blocks are enabled (`$wgCookieSetOnIpBlock`), after switching from one IP address to another the user might have blocks from the first IP address still applied. We also give all users (including anonymous users) the permissions necessary to perform all the actions in the script, to see that they are actually blocked from performing them rather than it just being a permissions issue.
```
$wgCookieSetOnIpBlock = false;
$wgGroupPermissions['*']['delete'] = true;
$wgGroupPermissions['*']['undelete'] = true;
$wgGroupPermissions['*']['block'] = true;
$wgGroupPermissions['*']['unblock'] = true;
$wgGroupPermissions['*']['unblockself'] = true;
$wgGroupPermissions['*']['globalblock'] = true;
$wgGroupPermissions['*']['sendemail'] = true;
$wgGroupPermissions['*']['move'] = true;
$wgGroupPermissions['*']['protect'] = true;
$wgGroupPermissions['*']['deleterevision'] = true;
$wgGroupPermissions['*']['rollback'] = true;
$wgGroupPermissions['*']['changetags'] = true;
$wgGroupPermissions['*']['applychangetags'] = true;
$wgGroupPermissions['*']['managechangetags'] = true;
$wgGroupPermissions['*']['centralauth-unmerge'] = true;
$wgRateLimits['thanks-notification']['anon'] = [ 100, 1 ];
$wgRateLimits['thanks-notification']['user'] = [ 100, 1 ];
$wgGroupPermissions['*']['upload'] = true;
$wgGroupPermissions['*']['upload_by_url'] = true;
$wgEnableUploads = true;
$wgAllowCopyUploads = true;
```
6. Enable uploads by following the instructions here https://www.mediawiki.org/wiki/Manual:Image_administration#Licensing.
7. To perform actions as an anonymous user from the IPs blocked by the `all_block_combinations.py` script, run:
```
python3 pwb.py all_actions_user -i 1.1.1.1 2.1.1.1 3.1.1.1 4.1.1.1 5.1.1.1 6.1.1.1 7.1.1.1 8.1.1.1 9.1.1.1 10.1.1.1 11.1.1.1 12.1.1.1 13.1.1.1 14.1.1.1 15.1.1.1 16.1.1.1 17.1.1.1 18.1.1.1 19.1.1.1 20.1.1.1 21.1.1.1 22.1.1.1 23.1.1.1 24.1.1.1 25.1.1.1 26.1.1.1 27.1.1.1 28.1.1.1 29.1.1.1 30.1.1.1 31.1.1.1 32.1.1.1 33.1.1.1 34.1.1.1 35.1.1.1 36.1.1.1 37.1.1.1 38.1.1.1 39.1.1.1 40.1.1.1 41.1.1.1 42.1.1.1 43.1.1.1 44.1.1.1 45.1.1.1 46.1.1.1 47.1.1.1 48.1.1.1 49.1.1.1 50.1.1.1 51.1.1.1 52.1.1.1 53.1.1.1 54.1.1.1 55.1.1.1 56.1.1.1 57.1.1.1 58.1.1.1 59.1.1.1 60.1.1.1 61.1.1.1 62.1.1.1 63.1.1.1 64.1.1.1 -a -lang:en -family:dockerwiki
```
8. To perform actions as a logged in user (below we use Admin) from the IPs blocked by the `all_block_combinations.py` script, run:
```
python3 pwb.py all_actions_user -i 1.1.1.1 2.1.1.1 3.1.1.1 4.1.1.1 5.1.1.1 6.1.1.1 7.1.1.1 8.1.1.1 9.1.1.1 10.1.1.1 11.1.1.1 12.1.1.1 13.1.1.1 14.1.1.1 15.1.1.1 16.1.1.1 17.1.1.1 18.1.1.1 19.1.1.1 20.1.1.1 21.1.1.1 22.1.1.1 23.1.1.1 24.1.1.1 25.1.1.1 26.1.1.1 27.1.1.1 28.1.1.1 29.1.1.1 30.1.1.1 31.1.1.1 32.1.1.1 33.1.1.1 34.1.1.1 35.1.1.1 36.1.1.1 37.1.1.1 38.1.1.1 39.1.1.1 40.1.1.1 41.1.1.1 42.1.1.1 43.1.1.1 44.1.1.1 45.1.1.1 46.1.1.1 47.1.1.1 48.1.1.1 49.1.1.1 50.1.1.1 51.1.1.1 52.1.1.1 53.1.1.1 54.1.1.1 55.1.1.1 56.1.1.1 57.1.1.1 58.1.1.1 59.1.1.1 60.1.1.1 61.1.1.1 62.1.1.1 63.1.1.1 64.1.1.1 -user:Admin -lang:en -family:dockerwiki
```
9. To test XFF IP blocks, comment out or remove the lines in LocalSettings.php you added in step 4, add `$wgApplyIpBlocksToXff = true;` and then run the commands in step 7 and/or 8 again
10. CSV files will be generated of the form `all_actions_user_<wiki>_<username or anonymous>_<date>.csv` which you can review to see which actions were blocked

## Analysing the results

After running `all_block_combinations.py` and `all_actions_user.py` you should have a file `all_block_combinations_<wiki>_<date>.csv` and one or more files `all_actions_user_<wiki>_<username or anonymous>_<date>.csv`. These can be combined to more easily analyse the results across all the runs.

In R, run these commands:
```
library(tidyr)

block <- read.csv("all_block_combinations_<wiki>_<date>.csv")
blocks <- data.frame(pivot_wider(block, id_cols=ip, names_from=c(type, anon), values_from=anon))
blocks <- merge(blocks, aggregate(list(anon=block$anon), by=list(ip=block$ip), FUN=all))

actions <- rbind(read.csv("all_actions_user_<wiki>_<username or anonymous>_<date>.csv"), read.csv("all_actions_user_<wiki>_<username or anonymous>_<date>.csv"), read.csv("all_actions_user_<wiki>_<username or anonymous>_<date>.csv"), ...)
actions <- data.frame(pivot_wider(actions, id_cols=ip, names_from=c(user, action), values_from=output))

write.csv(merge(blocks, actions, all=TRUE), "all_actions_user_combined.csv")
```

It will combine all the results into a CSV called `all_actions_user_combined.csv`. Each row in the CSV corresponds to an IP address, the columns respond to the actions each user took and the cells are the outcomes (e.g. blocked, permissions error, success, etc.)
